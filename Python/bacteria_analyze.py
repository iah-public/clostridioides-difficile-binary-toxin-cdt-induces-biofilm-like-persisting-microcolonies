#!/opt/anaconda3/envs/jmezatorres/bin/python3

import argparse
import glob
import os

import pandas as pd

from bacteria_quant import analyze_file, now

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Analyze MIP TIF files generated from "bacteria_register" and save the analysis results to a XSLX file.')
    parser.add_argument('paths', nargs='+', help='Paths to TIF files. If you provide a list of folders, all TIF files in the folders and subfolders will be processed.')
    parser.add_argument('--force-redo', action=argparse.BooleanOptionalAction, help='If set, will force redoing the analysis even if an analysis result can be found for an input image.')
    parser.add_argument('--do-tissue-segmentation', action=argparse.BooleanOptionalAction, help='If set, will perform tissue segmentation based on the actin and DAPI channels. If not, we suppose the image is entirely covered by tissue.')
    parser.add_argument('--min-obj-size', default=200, type=int, help='Minimal object size, in pixels, below which to reject bacteria detections.')
    args = parser.parse_args()

    do_tissue_segmentation = (args.do_tissue_segmentation is not None) and args.do_tissue_segmentation
    folders = list(args.paths)
    folders.sort()

    for folder in folders:

        if not os.path.exists(folder):
            print('\n%s - Error. File %s does not exist.' % ( now(), folder ))
            continue
        
        paths = []
        if os.path.isdir(folder):
            glob_folder = os.path.join(folder, '**/*-mip.tif')
            for p in glob.glob(glob_folder, recursive=True):
                paths.append(p)
        else:
            paths.append(folder)

        paths.sort()
        for p in paths:
            # Always a TIF file.            
            save_folder, image_filename = os.path.split(p)

            # Try to load the results file.
            bacteria_excel_file         = os.path.join(save_folder, 'results.xlsx')
            if not os.path.exists(bacteria_excel_file):
                df = None
            else:
                df = pd.read_excel(bacteria_excel_file)

            # ANALYZE.
            if df is None:
                to_analyze = True
            else:
                if args.force_redo or ('image' not in df.columns) or (image_filename not in df['image'].values):
                    to_analyze = True
                else:
                    to_analyze = False

            if to_analyze:
                print('Analyzing %s' % p)

                tissue_surface, mucin_mean, n_bacteria, n_mucin_positive_bacteria, bacteria_surface = analyze_file(
                    p, 
                    min_obj_size=args.min_obj_size, 
                    do_tissue_segmentation=do_tissue_segmentation)
                row = {}
                row['image']                        = image_filename 
                row['mucin_mean']                   = mucin_mean 
                row['tissue_surface']               = tissue_surface
                row['n_bacteria']                   = n_bacteria
                row['n_mucin_positive_bacteria']    = n_mucin_positive_bacteria
                row['bacteria_surface']             = bacteria_surface

                if df is None:
                    df = pd.DataFrame(row, index=[0])
                else:
                    if ('image' not in df.columns) or (image_filename not in df['image'].values):
                        row_df = pd.DataFrame(row, index=[0])
                        df = pd.concat([df, row_df], ignore_index=True)
                    else:
                        index = df[df['image'] == image_filename].index.item()
                        df.iloc[index] = row

                df.to_excel(bacteria_excel_file, index=False)
            else:
                print('Already analyzed: %s - Skipping.' % p)

    print('\n%s - Finished.' % now())

