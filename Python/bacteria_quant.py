import datetime
import os
from termcolor import colored
from nd2reader import ND2Reader
from nd2reader.exceptions import EmptyFileError
from skimage import measure
from skimage import transform
from skimage import morphology
from skimage import registration
from skimage import filters
import numpy as np
import pandas as pd
import tifffile
from roifile import ImagejRoi
from scipy.ndimage import fourier_shift
from cellpose import core, models
import imagej_luts
import logging
logging.getLogger("cellpose").setLevel(logging.CRITICAL)
#---------------------------------------------------------------------------------

# In what order will the channels be in the output TIF file.
dapi_channel =  0
mucin_channel = 1
actin_channel = 2
bact_channel =  3

#---------------------------------------------------------------------------------


def rearrange_channels(mip_path, current_channel_order):
    """
    Rearrange the channels in the MIP file.
    """
    mip, dx = reload_mip(mip_path)
    mip = [mip[i] for i in current_channel_order]
    write_mip_file(mip, mip_path, dx=dx)


def load_downsample(path, channel_order, scale=None, bad_zs=None):

    try:
        with ND2Reader(path) as images:
            images.bundle_axes = 'zyx'
            images.iter_axes = 't'
            if images.sizes['c'] != 4:
                print(colored('    File does not have 4 channels, but %d. Skipping.' % images.sizes['c'], 'red'))
                return None, None
            
            imgs = []
            for i in range(4):
                c = channel_order[i]
                if c < 0:
                    continue
                images.default_coords['c'] = c
                imgs.append(images[0])
            dx = images.metadata['pixel_microns']
    except EmptyFileError as efe:
        print(colored('    Problem reading file: %s' % efe, 'red'))
        return None, None


    if scale is not None:
        # Rescale.
        for i in range(len(imgs)):
            img = imgs[i]
            img = transform.rescale(img, [1., scale, scale], anti_aliasing=True)
            imgs[i] = img
        dx = dx / scale

    # Crop bad z-slices.
    if bad_zs is not None:
        for i in range(len(imgs)):
            img = imgs[i]
            img = img[bad_zs:-1, :, :]
            imgs[i] = img

    # Project.
    mips = []
    for img in imgs:
        mips.append(np.amax(img, 0))
    
    return mips, dx


def re_register_mip(mip_path, shifts, max_acceptable_shift):
    """
    Re-register the MIP file.
    """
    mip, dx = reload_mip(mip_path)
    mucin_mip   = mip[mucin_channel]
    bact_mip    = mip[bact_channel]
    bact_mip, shifts = register(bact_mip, mucin_mip, shifts=shifts, mask=None, max_acceptable_shift=max_acceptable_shift)
    mip[bact_channel] = bact_mip
    write_mip_file(mip, mip_path, dx=dx)
    return shifts


def register(bact_mip, mucin_mip, shifts, mask, max_acceptable_shift):
    """ Register the bacteria MIP image with respect to the mucin image.  """
    if shifts is None:
        shifts = registration.phase_cross_correlation(
            mucin_mip, bact_mip, upsample_factor=5, reference_mask=mask)
        shifts = shifts[0]
        if max(abs(shifts)) >= max_acceptable_shift:
            # Most likely it fails. Redo it without the mask.
            shifts, error, phase_diff = registration.phase_cross_correlation(
                mucin_mip, bact_mip, upsample_factor=5)
            
            if max(abs(shifts)) >=  max_acceptable_shift:
                print('    Registration failed with computed shifts = %s. Skipping registration.' % shifts)
                return bact_mip, [0,0]

    bact_mip = fourier_shift(np.fft.fftn(bact_mip), shifts)
    bact_mip = np.fft.ifftn(bact_mip)
    bact_mip = np.real(bact_mip)
    bact_mip = np.uint16(bact_mip)
    return bact_mip, shifts

def segment_bacteria_omnipose(bact_mip):
    use_GPU = core.use_gpu()
    model = models.CellposeModel(
        gpu=use_GPU, pretrained_model='/Users/tinevez/.cellpose/models/bact_fluor_omnitorch_0')
    chans = [0, 0]
    # define parameters
    mask_threshold = -1
    verbose = 0  # turn on if you want to see more output
    transparency = True  # transparency in flow output
    rescale = None  # give this a number if you need to upscale or downscale your images
    omni = True  # we can turn off Omnipose mask reconstruction, not advised
    # default is .4, but only needed if there are spurious masks to clean up; slows down output
    flow_threshold = 0
    resample = True  # whether or not to run dynamics on rescaled grid or original grid

    masks, flows, styles = model.eval(bact_mip,
                                      channels=chans,
                                      rescale=rescale,
                                      mask_threshold=mask_threshold,
                                      transparency=transparency,
                                      flow_threshold=flow_threshold,
                                      omni=omni,
                                      resample=resample,
                                      verbose=verbose)

    return masks

def get_object_properties(bact_labels, mucin_mip):
    """
    Get object properties and filter them out.
    """
    df = pd.DataFrame(
        measure.regionprops_table(
            bact_labels, mucin_mip,
            properties=['label', 'centroid', 'area',
                        'mean_intensity', 'solidity'],
        )
    ).set_index('label')
    return df

def write_mip_file(stack, mip_save_path,
                   dx=1.,
                   contrast_limits=(400., 5000., 200.0, 2000.0, 800., 10000., 100.0, 3000.0,  0., 256, 0., 256.),
                   luts=[imagej_luts.blue(), imagej_luts.green(), imagej_luts.red(), imagej_luts.magenta(), imagej_luts.glasbey_dark(), imagej_luts.glasbey_dark()],
                   labels=['DAPI', 'Mucin', 'Actin', 'Bacteria', 'Segmentation', 'Filtered'],
                   overlays=None
                   ):
    mip = np.stack(stack)
    mip = np.uint16(mip)
    n = len(stack)
    metadata = {
        'unit':    'um',
        'axes':    'CYX',
        'Ranges':  contrast_limits[0:2*n],
        'LUTs':    luts,
        'Labels':  labels[0:n],        
        'mode':    'composite'}
    if overlays is not None:
        metadata['Overlays'] = overlays
    tifffile.imwrite(mip_save_path, mip,
                     imagej=True,
                     resolution=(1./dx, 1./dx),
                     metadata=metadata)


def reload_mip(path):
    with tifffile.TiffFile(path) as tif:
        data = tif.asarray()
        axes = tif.series[0].axes
        channel_axis = axes.index('C')
        mips = []
        for c in range(min(4, data.shape[channel_axis])):
            mips.append(data.take(indices=c, axis=channel_axis))

        xres = tif.pages[0].tags['XResolution']
        dx = 1 / (xres.value[0]/xres.value[1])
        return mips, dx

def get_tissue_mask(mips):
    # Normalize and sum the DAPI and actin signal.
    dapi_mip    = 1. * mips[dapi_channel]
    actin_mip   = 1. * mips[actin_channel]
    imgs = [ dapi_mip, actin_mip ]
    mean_img = None
    for img in imgs:
        minv = np.amin(img)
        maxv = np.amax(img)
        rescaled = (img-minv) / (maxv-minv)
        if mean_img is None:
            mean_img = rescaled
        else:
            mean_img = mean_img + rescaled
    mean_img = mean_img / len(imgs)

    # Create mask. Fuuuuudge factors.
    mask_threshold = 0.7 * filters.threshold_otsu(mean_img)
    tissue_mask = mean_img > mask_threshold
    tissue_mask = morphology.remove_small_holes(tissue_mask, 50_000)
    tissue_mask = morphology.opening(tissue_mask, morphology.disk(10))

    # Only keep the largest object.
    labels = measure.label(tissue_mask)
    props = measure.regionprops(labels)
    areas = [p['area'] for p in props]
    largest = np.argmax(areas)
    tissue_mask = labels == (largest+1)
    
    return tissue_mask

def get_mucin_signal(mucin_mip, tissue_mask):

    # Do we have a tissue mask with an outside?
    have_outside = not np.all(tissue_mask)
    if have_outside:
        # Mean of the outside part.
        outside = np.mean(mucin_mip[~tissue_mask])
    else:
        # Min of the mucin data.
        outside = np.min(mucin_mip)
    inside  = np.sum(mucin_mip[tissue_mask]-outside)
    std     = np.std(mucin_mip[tissue_mask]-outside)
    size    = np.sum(tissue_mask)
    return inside/size, outside, std


def to_imagej_rois(contours, color=b'\xFF\xFF\x00', name=''):
    rois_bytes = []
    for i in range(len(contours)):
        contour = contours[i]

        # Swap X & Y for
        contour_ij = contour[:,[1, 0]]

        # Create ROI and name it.
        roi = ImagejRoi.frompoints(contour_ij, name +"_%d" % i)

        # Type.
        roi.roitype = ImagejRoi.roitype.FREELINE

        # Random color.
        roi.stroke_color = color
        rois_bytes.append(roi.tobytes())
    
    return rois_bytes

def labels_to_contours(labels):
    props = measure.regionprops(labels)
    pad = 1
    all_contours = []
    for p in props:
        label_mask = p['image_filled']
        padded_mask = np.pad(label_mask, pad)

        contours = measure.find_contours(padded_mask, 0.5, fully_connected='high')
        if (contours is None) or (len(contours) == 0):
            continue

        bbox = p['bbox']
        for contour in contours:
            contour[:,0] = contour[:,0] + bbox[0] - pad
            contour[:,1] = contour[:,1] + bbox[1] - pad
        all_contours = all_contours + contours
    return all_contours

def analyze_file(
    path,
    display=False,
    min_obj_size=200,  # Too small to be a bacteria, in pixels
    min_obj_solidity=0.7,   # Too irregular to be a bacteria
    contrast_limits=[[100., 10000.], [200., 2000.]],
    do_tissue_segmentation=False # Perform tissue segmentation?
):

    # Load image.
    mips, dx = reload_mip(path)

    # Segment tissue.
    if do_tissue_segmentation:
        tissue_mask = get_tissue_mask(mips)
        # Measure tissue size.
        tissue_surface = np.sum(tissue_mask) * dx * dx # µm2
    else:
        tissue_mask = np.ones_like(mips[0], dtype=bool)
        tissue_surface = mips[0].shape[0] * mips[0].shape[1] * dx * dx 

    # Create ROI from tissue mask.
    tissue_contours = measure.find_contours(tissue_mask, 0.5)
    tissue_rois = to_imagej_rois(tissue_contours, name='tissue')

    # Measure mucin signal
    mucin_mean, mucin_bg, mucin_std = get_mucin_signal(mips[mucin_channel], tissue_mask)

    # Bacteria segmentation in 2D.

    # Do we have a 4th channel with bacteria?
    if len(mips) < 4:

        n_bacteria = 0
        n_mucin_positive_bacteria = 0
        bacteria_surface = 0
        bact_positive_rois = []
        bact_negative_rois = []

    else:

        bact_mip = mips[bact_channel]
        bact_labels = segment_bacteria_omnipose(bact_mip)

        # Filter small objects.
        bact_labels = morphology.remove_small_objects(bact_labels, min_size=min_obj_size)

        # Bacteria total surface.
        bact_mask = bact_labels > 0
        bacteria_surface = np.sum(bact_mask) * dx * dx

        # Count them.
        props = measure.regionprops(bact_labels, mips[mucin_channel])
        n_bacteria = len(props)

        # Count mucin-positive bacteria.
        mucin_threshold = mucin_bg + mucin_mean + mucin_std
        n_mucin_positive_bacteria = 0
        bact_indices_positive = []
        bact_indices_negative = []
        for i in range(len(props)):
            p = props[i]
            val = p['intensity_mean']
            if val > mucin_threshold:
                n_mucin_positive_bacteria = n_mucin_positive_bacteria + 1
                bact_indices_positive.append(i)
            else:
                bact_indices_negative.append(i)
        
        # Create labels for positive bacteria.
        bact_labels_positive = np.zeros_like(bact_labels)
        for li in bact_indices_positive:
            p = props[li]
            coords = p['coords']
            bact_labels_positive[coords[:,0], coords[:,1]] = li
        
        # Create ROIs from postive bacteria labels.
        bact_positive_contours = labels_to_contours(bact_labels_positive)
        bact_positive_rois = to_imagej_rois(bact_positive_contours, b'\xFF\xFF\xFF', name='mucin_pos_bacteria')
        
        # Create labels for negative bacteria.
        bact_labels_negative = np.zeros_like(bact_labels)
        for li in bact_indices_negative:
            p = props[li]
            coords = p['coords']
            bact_labels_negative[coords[:,0], coords[:,1]] = li
        
        # Create ROIs from postive bacteria labels.
        bact_negative_contours = labels_to_contours(bact_labels_negative)
        bact_negative_rois = to_imagej_rois(bact_negative_contours, b'\xFF\x00\x00', name='mucin_neg_bacteria')

    # Resave the MIP with ROIs.
    write_mip_file(mips, path, dx, overlays= tissue_rois+bact_positive_rois+bact_negative_rois)

    # Return the data.
    return tissue_surface, mucin_mean, n_bacteria, n_mucin_positive_bacteria, bacteria_surface


def to_mip_save_path(raw_file_save_path):
    # Deal with file paths.
    root_folder, image_filename = os.path.split(raw_file_save_path)
    save_folder = os.path.join(root_folder, 'Results')
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    # Save path
    save_filename = image_filename.replace('.nd2', '-mip.tif')
    mip_save_path = os.path.join(save_folder, save_filename)
    return mip_save_path

def load_and_resave(path,
                    shifts=None,
                    force_redo=False  # Redo analysis even if we find a MIP file?
                    ):

    # Where to save the resulting MIP.
    mip_save_path = to_mip_save_path(path)

    if (not force_redo) and os.path.exists(mip_save_path):
        return False

    else:
        # Load & downsample image.
        mips, dx = load_downsample(path)

        # Register bacteria channel.
        mask = get_tissue_mask(mips)
        mucin_mip   = mips[mucin_channel]
        bact_mip    = mips[bact_channel]
        bact_mip = register(bact_mip, mucin_mip, shifts, mask)
        mips[bact_channel] = bact_mip

        # Resave them for later.
        write_mip_file(mips, mip_save_path, dx=dx)
        return True

def now():
    now = datetime.datetime.now()
    return (now.strftime("%Y-%m-%d %H:%M:%S"))
