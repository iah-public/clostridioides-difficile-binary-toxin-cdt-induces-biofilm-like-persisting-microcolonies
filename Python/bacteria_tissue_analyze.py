#!/opt/anaconda3/envs/jmezatorres/bin/python3

import argparse
import glob
import os
import pandas as pd
from bacteria_quant import now, reload_mip, get_tissue_mask, to_imagej_rois, get_mucin_signal, mucin_channel, bact_channel, write_mip_file
import numpy as np
from skimage import measure
from skimage.filters import median, gaussian
from skimage.morphology import square, remove_small_objects


def analyze_file( path, do_tissue_segmentation=True, threshold_bact=450):

    # Load image.
    mips, dx = reload_mip(path)

    # Segment tissue.
    if do_tissue_segmentation:
        tissue_mask = get_tissue_mask(mips)
        # Measure tissue size.
        tissue_surface = np.sum(tissue_mask) * dx * dx # µm2
    else:
        tissue_mask = np.ones_like(mips[0], dtype=bool)
        tissue_surface = mips[0].shape[0] * mips[0].shape[1] * dx * dx 

    # Create ROI from tissue mask.
    tissue_contours = measure.find_contours(tissue_mask, 0.5)
    tissue_rois = to_imagej_rois(tissue_contours, name='tissue')

    # Measure mucin signal
    mucin_mip = mips[mucin_channel]
    inside  = np.sum(mucin_mip[tissue_mask])
    size    = np.sum(tissue_mask)
    tissue_mucin_mean = inside / size

    # Get bacteria channel
    bc = mips[bact_channel]

    # Filter with median then gaussian.
    bc = median(bc, square(9))
    bc = gaussian(bc, sigma=0.5, preserve_range=True)

    # Set to 0 outside tissue.
    bc[~tissue_mask] = 0

    # Threshold bacteria.
    bact_mask = bc > threshold_bact

    # Discard objects smaller than 2 µm2.
    bact_mask = measure.label(bact_mask, background=0)
    bact_mask = bact_mask.astype(bool)
    bact_mask = remove_small_objects(bact_mask, min_size=2/dx/dx)

    # Is it empty?
    if np.sum(bact_mask) == 0:
        bact_mucin_mean = 0
        bacteria_surface = 0
        bact_rois = []
        bact_mucin_bg = 0
        bact_mucin_std = 0
    else:
        # Create ROIs from bacteria mask.
        bact_contours = measure.find_contours(bact_mask, 0.5)
        bact_rois = to_imagej_rois(bact_contours, b'\xFF\xFF\xFF', name='bacteria')

        # Measure bacteria size.
        bacteria_surface = np.sum(bact_mask) * dx * dx # µm2

        # Measure mucin signal in bacteria.
        inside = np.sum(mucin_mip[bact_mask])
        size = np.sum(bact_mask)
        bact_mucin_mean = inside / size

    # Resave the MIP with ROIs.
    write_mip_file(mips, path, dx, overlays=tissue_rois+bact_rois, contrast_limits=(400., 5000., 200.0, 20000.0, 100., 600., 0., 1000.0,  0., 256, 0., 256.))

    # Return
    return tissue_surface, bacteria_surface, tissue_mucin_mean, bact_mucin_mean



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Analyze MIP TIF files generated from "bacteria_register" and save the analysis results to a XSLX file. This script assumes the data comes from mice tissue, and simply segment bacteria surface vs tissue surface.')
    parser.add_argument('paths', nargs='+', help='Paths to TIF files. If you provide a list of folders, all TIF files in the folders and subfolders will be processed.')
    parser.add_argument('--force-redo', action=argparse.BooleanOptionalAction, help='If set, will force redoing the analysis even if an analysis result can be found for an input image.')
    parser.add_argument('--do-tissue-segmentation', action=argparse.BooleanOptionalAction, help='If set, will perform tissue segmentation based on the actin and DAPI channels. If not, we suppose the image is entirely covered by tissue.')
    args = parser.parse_args()

    do_tissue_segmentation = (args.do_tissue_segmentation is not None) and args.do_tissue_segmentation
    folders = list(args.paths)
    folders.sort()

    for folder in folders:

        if not os.path.exists(folder):
            print('\n%s - Error. File %s does not exist.' % ( now(), folder ))
            continue
        
        paths = []
        if os.path.isdir(folder):
            glob_folder = os.path.join(folder, '**/*-mip.tif')
            for p in glob.glob(glob_folder, recursive=True):
                paths.append(p)
        else:
            paths.append(folder)

        paths.sort()
        for p in paths:
            # Always a TIF file.            
            save_folder, image_filename = os.path.split(p)

            # Try to load the results file.
            bacteria_excel_file         = os.path.join(save_folder, 'results.xlsx')
            if not os.path.exists(bacteria_excel_file):
                df = None
            else:
                df = pd.read_excel(bacteria_excel_file)

            # ANALYZE.
            if df is None:
                to_analyze = True
            else:
                if args.force_redo or ('image' not in df.columns) or (image_filename not in df['image'].values):
                    to_analyze = True
                else:
                    to_analyze = False

            if to_analyze:
                print('Analyzing %s' % p)

                tissue_surface, bacteria_surface, tissue_mucin_mean, bact_mucin_mean = analyze_file(
                    p, 
                    do_tissue_segmentation=do_tissue_segmentation)
                row = {}
                row['image']                        = image_filename 
                row['tissue_mucin_mean']            = tissue_mucin_mean 
                row['bacteria_mucin_mean']          = bact_mucin_mean 
                row['tissue_surface']               = tissue_surface
                row['bacteria_surface']             = bacteria_surface

                if df is None:
                    df = pd.DataFrame(row, index=[0])
                else:
                    if ('image' not in df.columns) or (image_filename not in df['image'].values):
                        row_df = pd.DataFrame(row, index=[0])
                        df = pd.concat([df, row_df], ignore_index=True)
                    else:
                        index = df[df['image'] == image_filename].index.item()
                        df.iloc[index] = row

                df.to_excel(bacteria_excel_file, index=False)
            else:
                print('Already analyzed: %s - Skipping.' % p)

    print('\n%s - Finished.' % now())

