#!/opt/anaconda3/envs/jmezatorres/bin/python3

import os
import glob
from bacteria_quant import now
from bacteria_quant import re_register_mip

import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Read a MIP TIF file, and perform again registration.')
    parser.add_argument('paths', nargs='+', help='Paths to TIF files.')
    parser.add_argument('--dx', type=float)
    parser.add_argument('--dy', type=float)
    parser.add_argument('--remove-z-below', type=int)
    parser.add_argument('--max-acceptable-shift', type=float, default=200.)

    args = parser.parse_args()

    # Paths.
    folders = list(args.paths)
    folders.sort()

    # Shifts.
    if (args.dx is not None) and (args.dy is not None):
        specified_shifts = [ args.dy, args.dx ]
    else:
        specified_shifts = None

    # Max tolerable shift
    max_acceptable_shift = args.max_acceptable_shift        

    for folder in folders:

        if not os.path.exists(folder):
            print('%s - Error. TIF file %s does not exist.' % ( now(), folder ))
            continue
        
        paths = []
        if os.path.isdir(folder):
            glob_folder = os.path.join(folder, '*.nd2')
            for p in glob.glob(glob_folder):
                paths.append(p)
        else:
            paths.append(folder)

        paths.sort()
        for p in paths:

            print('%s - Processing file: %s' % ( now(), p ))
            re_register_mip(p, specified_shifts, max_acceptable_shift)

    print('%s - Finished.' % now())
