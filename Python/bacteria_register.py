#!/opt/anaconda3/envs/jmezatorres/bin/python3

import os
import glob
from bacteria_quant import now
from bacteria_quant import load_downsample
from bacteria_quant import get_tissue_mask
from bacteria_quant import register
from bacteria_quant import to_mip_save_path
from bacteria_quant import write_mip_file
from bacteria_quant import bact_channel
from bacteria_quant import mucin_channel

import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Read a ND2 file, project the image and register the mucin channel with respect to the bacteria channel."
    )
    parser.add_argument("paths", nargs="+", help="Paths to ND2 files. If you provide a folder, all ND2 files in the folder and subfolders will be processed.")
    parser.add_argument("--force-redo", action=argparse.BooleanOptionalAction)
    parser.add_argument("--save-mip", action=argparse.BooleanOptionalAction)
    parser.add_argument("--dx", type=float)
    parser.add_argument("--dy", type=float)
    parser.add_argument("--remove-z-below", type=int)
    parser.add_argument("--max-acceptable-shift", type=float, default=200.0)
    parser.add_argument(
        "--channel-dapi",
        type=int,
        default=0,
        help="Position of the DAPI channel in the source image, 0-indexed. Default: 0",
    )
    parser.add_argument(
        "--channel-mucin",
        type=int,
        default=1,
        help="Position of the mucin channel in the source image, 0-indexed. Default: 1",
    )
    parser.add_argument(
        "--channel-actin",
        type=int,
        default=2,
        help="Position of the actin channel in the source image, 0-indexed. Default: 2",
    )
    parser.add_argument(
        "--channel-bacteria",
        type=int,
        default=3,
        help="Position of the bacteria channel in the source image, 0-indexed. Default: 3",
    )
    args = parser.parse_args()

    # Channels.
    input_bact_channel = args.channel_bacteria
    input_mucin_channel = args.channel_mucin
    input_dapi_channel = args.channel_dapi
    input_actin_channel = args.channel_actin
    input_channel_order = [
        input_dapi_channel,
        input_mucin_channel,
        input_actin_channel,
        input_bact_channel,
    ]

    # Shifts.
    if (args.dx is not None) and (args.dy is not None):
        specified_shifts = [args.dy, args.dx]
    else:
        specified_shifts = None

    # Max tolerable shift
    max_acceptable_shift = args.max_acceptable_shift

    # Paths.
    folders = list(args.paths)
    folders.sort()

    for folder in folders:
        if not os.path.exists(folder):
            print("\n%s - Error. ND2 file %s does not exist." % (now(), folder))
            continue

        paths = []
        if os.path.isdir(folder):
            glob_folder = os.path.join(folder, "**/*.nd2")
            for p in glob.glob(glob_folder, recursive=True):
                paths.append(p)
        else:
            paths.append(folder)

        paths.sort()
        for p in paths:
            mip_save_path = to_mip_save_path(p)
            if not args.force_redo and os.path.exists(mip_save_path):
                print("\n%s - ND2 file %s already processed. Skipping." % (now(), p))
                continue

            print("\n%s - Processing ND2 file: %s" % (now(), p))
            print("    Converting and projecting raw file.")
            mips, dx = load_downsample(
                p, input_channel_order, bad_zs=args.remove_z_below
            )
            if mips is None:
                continue

            # Register 2nd channel.
            if input_mucin_channel >= 0 and input_bact_channel >= 0:
                mucin_mip = mips[mucin_channel]
                bact_mip = mips[bact_channel]

                if specified_shifts is None:
                    print("    Computing shifts.")
                else:
                    print("    Applying specified shifts: %s" % specified_shifts)

                mask = get_tissue_mask(mips)
                bact_mip, shifts = register(
                    bact_mip, mucin_mip, specified_shifts, mask, max_acceptable_shift
                )
                mips[bact_channel] = bact_mip
                print("    Applied shifts: %s" % shifts)
            else:
                print("    Skipping registration.")

            # Resave if asked to.
            if args.save_mip:
                print("    Saving corrected MIP file to: %s" % mip_save_path)
                write_mip_file(mips, mip_save_path, dx=dx)

    print("\n%s - Finished." % now())
