#!/opt/anaconda3/envs/jmezatorres/bin/python3

import os
import glob
from bacteria_quant import now
from bacteria_quant import rearrange_channels

import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Read a MIP TIF file, and re-arrange channels so that their order match the expected order for analysis.')
    parser.add_argument('paths', nargs='+', help='Paths to TIF files.')
    parser.add_argument('--channel-dapi', type=int, default=0, help='Position of the DAPI channel in the source image, 0-indexed. Default: 0')
    parser.add_argument('--channel-mucin', type=int, default=1, help='Position of the mucin channel in the source image, 0-indexed. Default: 1')
    parser.add_argument('--channel-actin', type=int, default=2, help='Position of the actin channel in the source image, 0-indexed. Default: 2')
    parser.add_argument('--channel-bacteria', type=int, default=3, help='Position of the bacteria channel in the source image, 0-indexed. Default: 3')
    args = parser.parse_args()

    # Channels.
    input_bact_channel = args.channel_bacteria
    input_mucin_channel = args.channel_mucin
    input_dapi_channel = args.channel_dapi
    input_actin_channel = args.channel_actin
    input_channel_order = [ input_dapi_channel, input_mucin_channel, input_actin_channel, input_bact_channel ]


    # Paths.
    folders = list(args.paths)
    folders.sort()

    for folder in folders:

        if not os.path.exists(folder):
            print('%s - Error. TIF file %s does not exist.' % ( now(), folder ))
            continue
        
        paths = []
        if os.path.isdir(folder):
            glob_folder = os.path.join(folder, '*.tif')
            for p in glob.glob(glob_folder):
                paths.append(p)
        else:
            paths.append(folder)

        paths.sort()
        for p in paths:

            print('%s - Processing file: %s' % ( now(), p ))
            rearrange_channels(p, input_channel_order)

    print('%s - Finished.' % now())
